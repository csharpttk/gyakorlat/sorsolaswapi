﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Sorsolas.Controllers
{
    public class SorsolasController : ApiController
    {
        public static List<string> nevek = new List<string>(){"Alma Andrea", "Balga Bálint","Bóbita Barna","Csini Cecilia", "Dunai Dániel", "Erdei Enikő","Favágó Ferenc",
                    "Gyöngyösi Gyöngyi","Hajnal Hajnalka","Kállai Kálmán","Lenkei Lilla","Mányoki Márton", "Nyúlfalvi Nóra","Oroszi Ottó","Pál Piroska" };
        [EnableCors(origins:"*", headers: "*", methods: "*")]
        public IHttpActionResult GetAll() //id paraméternév a default!
        {
            try
            {
                    return Ok(nevek);
            }
            catch
            {
                return NotFound();
            }

        }
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public IHttpActionResult GetKivalasztott(int id) //id-t nem használom - paraméter csak megkülönbözteti a hívásokat
        {
            try
            {
                Random r = new Random();                
                return Ok(nevek[r.Next(nevek.Count)]); //véletlenszerűen választott név
            } catch { 
                return NotFound();
            }
            
        }
    }
}
